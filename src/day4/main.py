from dataclasses import dataclass
from enum import Enum
from datetime import datetime
from typing import List, Tuple, Dict, Optional
from collections import defaultdict
import re

from util import Input, time_function


class Action(Enum):
    SLEEP = 'sleep'
    AWAKES = 'awakes'


@dataclass
class Log:
    timestamp: datetime
    action: Action
    guard_id: int

    def __str__(self):
        return f"<LOG {self.timestamp} | ({self.guard_id}) {self.action}>"


def clean_data(input: List[str]) -> List[Log]:
    regex = re.compile("\[(.*)\] (.*)")
    temp: List[Tuple[datetime, str]] = []
    for i in input:
        matches = regex.match(i).groups()
        temp.append((
            datetime.strptime(matches[0], "%Y-%m-%d %H:%M"),
            matches[1]
        ))

    temp.sort()

    output: List[Log] = []

    regex = re.compile(".*#(\d+).*")
    current_guard: int = 0
    for i in temp:
        try:
            matches = regex.match(i[1]).groups()
            current_guard = int(matches[0])
            continue
        except AttributeError as e:
            if i[1] == 'falls asleep':
                output.append(Log(
                    timestamp=i[0],
                    action=Action.SLEEP,
                    guard_id=current_guard
                ))
            elif i[1] == 'wakes up':
                output.append(Log(
                    timestamp=i[0],
                    action=Action.AWAKES,
                    guard_id=current_guard
                ))
            else:
                raise ValueError(i)

    return output


def calculate_time_asleep(input: List[Log]):

    time_asleep: Dict[int, int] = defaultdict(int)
    asleep_since: Optional[datetime] = None
    for i in input:
        if i.action == Action.SLEEP:
            asleep_since = i.timestamp
        if i.action == Action.AWAKES:
            time_asleep[i.guard_id] += (i.timestamp - asleep_since).seconds / 60
            asleep_since = None

    return time_asleep


def find_sleepy_minute(input: List[Log], guard_id: int) -> List[int]:
    stats_per_minute: List[int] = [0]*60

    asleep_since: Optional[datetime] = None
    for i in input:
        if i.guard_id != guard_id:
            continue

        if i.action == Action.SLEEP:
            asleep_since = i.timestamp
        if i.action == Action.AWAKES:
            for j in range(asleep_since.minute, i.timestamp.minute, 1):
                stats_per_minute[j] += 1

    return stats_per_minute


@time_function
def solve(filename: str):
    input = Input(filename).read(False)
    cleaned = clean_data(input)
    stats = calculate_time_asleep(cleaned)
    sleepy_guard = max(stats, key=stats.get)
    print(f"The guard that is most asleep is guard {sleepy_guard}")
    stats_per_minute = find_sleepy_minute(cleaned, guard_id=sleepy_guard)
    sleepy_minute = stats_per_minute.index(max(stats_per_minute))
    print(f"The guard is most of the times asleep at minute {sleepy_minute}")
    print(f"Answer of part 1: {sleepy_minute * sleepy_guard}")

    sleepy_minute_per_guard = {}
    for g in set([i.guard_id for i in cleaned]):
        stats_per_minute = find_sleepy_minute(cleaned, guard_id=g)
        sleepy_minute_per_guard[g] = max(stats_per_minute)

    max_sleepy_guard = max(sleepy_minute_per_guard, key=sleepy_minute_per_guard.get)
    print(f"The guard having the highest 'sleepy_minute' is guard {max_sleepy_guard}")

    stats_per_minute = find_sleepy_minute(cleaned, guard_id=max_sleepy_guard)
    sleepy_minute = stats_per_minute.index(max(stats_per_minute))
    print(f"And this guard is most of the times asleep at minute {sleepy_minute}")
    print(f"Answer of part 2: {sleepy_minute * max_sleepy_guard}")


if __name__ == '__main__':
    solve('input.txt')
