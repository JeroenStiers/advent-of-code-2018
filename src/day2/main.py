from typing import List
from collections import Counter
from itertools import combinations

from util import Input, time_function


def solve1(input):

    counts: List= [0, 0, 0, 0]
    for i in input:
        c = [j[1] for j in Counter(i).most_common()]

        if 3 in c:
            counts[3] += 1
        if 2 in c:
            counts[2] += 1

    return counts


def solve2(input):

    for i, j in combinations(input, 2):
        count_differences: int = 0
        for i_index, i_char in enumerate(i):
            if i_char != j[i_index]:
                count_differences += 1

        if count_differences == 1:
            print(f"{i} ~= {j}")
            return

@time_function
def solve(filename: str):
    input = Input(filename).read(False)
    result = solve1(input)
    print(f"Answer part 1: {result[2] * result[3]}")
    print(f"Answer part 2: {solve2(input)}")


if __name__ == '__main__':
    solve('input.txt')