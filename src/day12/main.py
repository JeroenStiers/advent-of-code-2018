from typing import *
from dataclasses import dataclass
from src.util import Input
from src.util import StringExtractor
from util import time_function


@dataclass
class Conversion:
    extractor: StringExtractor
    result: str

    def __str__(self) -> str:
        return f"Conversion(extractor={self.extractor}, result={self.result})"


def extract_info(input: List[str]) -> Tuple[str, List[Conversion]]:
    initial_state = input[0][15:]
    conversions: List[Conversion] = []

    extractor = StringExtractor(pattern=r'([.#]{5}) => ([.#])')
    for conversion in input[2:]:

        config, result = extractor.extract(conversion)
        if config[2] == result:
            continue
        conversions.append(Conversion(extractor=StringExtractor(pattern=config.replace('.', '\.')), result=result))
    return initial_state, conversions


@time_function
def solve(filename: str):
    input = Input(filename).read()
    initial_state, conversions = extract_info(input)
    current_state = initial_state

    for generation in range(1, 5):
        print(f"\n\nGeneration {generation}")
        start_chars = max(0, 4-current_state.index('#'))
        end_chars = max(0, 4-current_state[::-1].index("#"))
        current_state = f"{'.' * start_chars}{current_state}{'.' * end_chars}"
        new_state = [*current_state]

        print(current_state)

        for conversion in conversions:
            print(conversion)
            for match in conversion.extractor.finditer(current_state):
                print(f"\t{match.start()}-{match.end()} > {current_state[match.start():match.end()]}")
                new_state[match.start() + 2] = conversion.result
                print(f"\t{''.join(new_state)}")

        current_state = ''.join(new_state)
        print(current_state)


if __name__ == '__main__':
    solve('test.txt')