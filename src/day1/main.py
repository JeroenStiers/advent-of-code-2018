from typing import Set

from util import Input, time_function


@time_function
def solve(filename: str):
    input = Input(filename).read(True)
    print(f"Answer part 1: {sum(input)}")
    already_found: Set[int] = set()
    current_state: int = 0
    while True:
        for i in input:
            current_state += i

            if current_state in already_found:
                print(f"Answer part 2: {current_state}")
                return
            already_found.add(current_state)


if __name__ == '__main__':
    solve("input.txt")
