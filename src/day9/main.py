from typing import List

from src.util import Input
from src.util import StringExtractor


def get_new_position(current_position: int, distance: int, length_board: int) -> int:
    pass


def solve(players: int, last_marble: int):

    players: List[int] = [0 for i in range(players-1)]
    board: List[int] = []

    current_marble: int = 0
    current_position: int = 0

    while current_marble <= last_marble:

        print(f'current marble: {current_marble}')
        current_player: int = current_marble % len(players)
        print(f': current_player: {current_player}')
        current_marble += 1
        continue

        special_23: bool = False

        if current_marble % 23 == 0:
            special_23 = True

        new_position = get_new_position(current_position, 7 if special_23 else 1, len(board))

        if special_23:
            pass
        else:
            board.insert(new_position, current_marble)


        current_marble += 1

    return max(players)


if __name__ == '__main__':
    input = Input('test.txt').read()
    count_players, last_marble = StringExtractor(pattern=r'(\d+) players.*worth (\d+) points').extract(input[0])
    print(f"Result part 1: {solve(players=int(count_players), last_marble=int(last_marble))}")