from collections import defaultdict
from typing import *

from src.util import Input
from src.util import StringExtractor
from util import time_function


def react(input: str) -> str:
    state = input + '_'

    while True:
        new_state: str = ''
        reacting: bool = False
        for i, j in zip(state[:-1], state[1:]):
            # print(f"{i}{j}")
            if reacting:
                reacting = False
                continue

            if i.lower() != j.lower():
                new_state += i
                continue

            if i == j:
                new_state += i
                continue
            reacting = True

        if len(state) == len(new_state) + 1:
            break
        state = new_state + '_'
    return new_state


@time_function
def solve(filename: str):
    input = Input(filename).read()[0]
    print(f"Part 1: {len(react(input))}")

    # Get all the unit types:
    distinct_unit_types = list(set(input.lower()))
    memory: Dict[str, int] = defaultdict(int)

    for unit_type in distinct_unit_types:
        memory[unit_type] = len(react(input.replace(unit_type, '').replace(unit_type.upper(), '')))

    lowest = sorted(memory.items(), key=lambda item: item[1])[0]
    print(f"Part 2: {lowest[0]} -> len = {lowest[1]}")


if __name__ == '__main__':
    solve('input.txt')
