from abc import ABC, abstractmethod
from typing import List, Union, Set, Dict


class Node:

    def __init__(self, name: str):
        self.name: str = name
        self.incoming: Set[Node] = set()
        self.outgoing: Set[Node] = set()

    def __str__(self):
        return f"<NODE: {self.name}>"

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.name == other.name
        return False

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.name)


class Network(ABC):

    def __init__(self):
        self.network: Dict[str, Node] = {}

    def get_node(self, name: str) -> Node:
        try:
            return self.network[name]
        except KeyError:
            # If the node does not exist yet, create it and add it to the network
            new_node = Node(name=name)
            self.network[name] = new_node
            return new_node

    @abstractmethod
    def add_connection(self, name1: str, name2:str) -> None:
        pass


class DirectionalNetwork(Network):

    def add_connection(self, name1: str, name2: str) -> None:

        node1 = self.get_node(name1)
        node2 = self.get_node(name2)
        node1.outgoing.add(node2)
        node2.incoming.add(node1)


class NonDirectionalNetwork(Network):

    def add_connection(self, name1: str, name2: str) -> None:
        node1 = self.get_node(name1)
        node2 = self.get_node(name2)
        node1.outgoing.add(node2)
        node1.incoming.add(node1)
        node2.outgoing.add(node1)
        node2.incoming.add(node1)


class ReachabilityDefinition(ABC):
    """Defines the rules to calculate an accessible node"""

    @abstractmethod
    def get_reachable_nodes(self, network: Network, visited_nodes: Set[Node]) -> Set[Node]:
        pass


class ExhaustiveReachability(ReachabilityDefinition):
    """All incoming nodes have to be visited before this node is accessible"""

    def get_reachable_nodes(self, network: Network, visited_nodes: Set[Node]) -> Set[Node]:
        accessible_nodes: Set[Node] = set()
        for node in network.network.values():
            if len(node.incoming) == 0 or node.incoming.issubset(visited_nodes):
                accessible_nodes.add(node)
        return accessible_nodes


class GreedyReachability(ReachabilityDefinition):
    """If one of the incoming node is visited, this node is accessible"""

    def get_reachable_nodes(self, network: Network, visited_nodes: Set[Node]) -> Set[Node]:
        accessible_nodes: Set[Node] = set()
        for node in network.network.values():
            if len(node.incoming) == 0 or not node.incoming.isdisjoint(visited_nodes):
                accessible_nodes.add(node)
        return accessible_nodes


class Graph:
    """Provides functionality to work with a non directional graph. Provide a different reachability_methods to
    alter the functionality defining the reachable nodes"""

    def __init__(self, network: Network = NonDirectionalNetwork(),
                 reachability_definition: ReachabilityDefinition = GreedyReachability()):
        self.__visited_nodes: Set[Node] = set()
        self.__network: Network = network
        self.__reachability_definition: ReachabilityDefinition = reachability_definition

    def __get_node(self, name: str) -> Node:
        return self.__network.get_node(name)

    def add_connection(self, name1: str, name2: str):
        self.__network.add_connection(name1, name2)

    def visit(self, node: str | Node) -> Node:
        if isinstance(node, str):
            node = self.__get_node(name=node)

        self.__visited_nodes.add(node)
        return node

    def get_reachable_nodes(self, exclude_visited: bool = True) -> List[Node]:
        accessible_nodes: Set[Node] = self.__reachability_definition.get_reachable_nodes(self.__network, self.__visited_nodes)

        if exclude_visited:
            return list(accessible_nodes - self.__visited_nodes)
        return list(accessible_nodes)
