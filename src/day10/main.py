from typing import List, Set

from src.util import Input
from src.util import Position, Velocity, GridAgent, GridSpace
from src.util import StringExtractor
from util import time_function


def generate_grid(input: List[str]) -> GridSpace:

    grid = GridSpace()
    extractor = StringExtractor(pattern=r"position=< *(-?\d+), *(-?\d+)> velocity=< *(-?\d+), *(-?\d+)>")

    for i in input:
        pos_x, pos_y, vel_x, vel_y = extractor.extract(i)
        grid.add_agent(GridAgent(position=Position(int(pos_x), int(pos_y)), velocity=Velocity(int(vel_x), int(vel_y))))

    return grid


@time_function
def solve(filename: str):
    input = Input(filename).read()
    grid = generate_grid(input)

    minimum_extent_time = grid.find_time_with_minimum_area()
    grid.print()
    grid.write_to_textfile()


if __name__ == '__main__':
    solve('input.txt')
