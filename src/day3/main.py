from collections import defaultdict
from typing import Dict, List, Set

from src.util import Input
from util import time_function, GridSpace, StringExtractor, GridAgent, Position
from util.grid.grid_space import Size


def parse_input(input):
    gs = GridSpace()

    se = StringExtractor(pattern=r'#(\d+) @ (\d+),(\d+): (\d+)x(\d+)')
    for i in input:
        id, left, top, width, height = se.extract(i)
        gs.add_agent(GridAgent(
            name=id,
            position=Position(col=int(left), row=int(top)),
            size=Size(width=int(width), height=int(height))
        ))
    return gs


def analyse_claims(gs: GridSpace) -> Dict[Position, Set[str]]:

    occupied_positions: Dict[Position, Set[str]] = defaultdict(set)

    for a in gs.agents:
        for pos in a.extent.iterate_positions():
            occupied_positions[pos].add(a.name)

    return occupied_positions


@time_function
def solve(filename: str):
    input = Input(filename).read()
    gs = parse_input(input)

    occupied_positions = analyse_claims(gs)
    print(f"The number of positions overlapping at least twice: {len([1 for i in occupied_positions.values() if len(i) >= 2])}")

    options: List[str] = [a.name for a in gs.agents]
    for ids in occupied_positions.values():
        if len(ids) == 1:
            continue

        for id in ids:
            try:
                options.remove(id)
            except ValueError:
                # Is no longer in list
                pass
    print(f"The id of the claims that are not overlapping with any other claim: {', '.join(options)}")


if __name__ == '__main__':
    solve('input.txt')
