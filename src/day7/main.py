from typing import List, Set

from src.util import Input
from src.util import Graph, ExhaustiveReachability, Node, DirectionalNetwork
from src.util import StringExtractor
from util import time_function


def build_graph(input) -> Graph:
    g = Graph(network=DirectionalNetwork(), reachability_definition=ExhaustiveReachability())
    se = StringExtractor("Step ([A-Z]+) .* before step ([A-Z]+) can begin\.")

    for i in input:
        from_, to = se.extract(i)
        g.add_connection(name1=from_, name2=to)
    return g


def solve1(g: Graph) -> str:

    order_of_visits: str = ""
    while True:
        nodes: List[Node] = g.get_reachable_nodes()

        if len(nodes) == 0:
            return order_of_visits

        nodes.sort(key=lambda x: x.name)
        order_of_visits += nodes[0].name
        g.visit(nodes[0])


class Worker:

    def __init__(self, id: int):
        self.id: int = id
        self.name_task: str | None = None
        self.duration_task: int = 0

    def assign_task(self, name: str, duration: int):
        self.name_task = name
        self.duration_task = duration

    def process_step(self):
        if not self.idle:
            self.duration_task -= 1

    @property
    def idle(self) -> bool:
        return self.duration_task == 0

    @property
    def done(self) -> bool:
        return self.duration_task == 0 and self.name_task is not None


def solve2(g: Graph) -> int:
    order = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    workers: List[Worker] = [Worker(1), Worker(2), Worker(3), Worker(4), Worker(5)]
    total_duration: int = 0
    tasks_assigned: Set[str] = set()

    while True:

        for w in [i for i in workers if i.done]:
            g.visit(w.name_task)
            w.name_task = None

        reachable_nodes: List[Node] = g.get_reachable_nodes()
        reachable_nodes = list(set(reachable_nodes) - tasks_assigned)
        reachable_nodes.sort(key=lambda x: x.name)

        for w in workers:
            if w.idle and len(reachable_nodes) > 0:
                node_to_assign = reachable_nodes.pop(0)
                w.assign_task(
                    name=node_to_assign.name,
                    duration=60 + order.index(node_to_assign.name)
                )
                tasks_assigned.add(node_to_assign)

        if len(g.get_reachable_nodes()) == 0 and not any([not w.idle for w in workers]):
            return total_duration

        # Process the step
        [w.process_step() for w in workers]
        total_duration += 1

        # print(f"{total_duration:^10}{(workers[0].name_task or ''):^6}{(workers[1].name_task or ''):^6}")


@time_function
def solve(filename: str):
    input = Input(filename).read()
    result = solve1(build_graph(input))
    print(f"Result part 1 - order of solving: {result}")
    result = solve2(build_graph(input))
    print(f"Result part 2 - total duration: {result}")


if __name__ == '__main__':
    solve('input.txt')